import { Routes } from '@angular/router';

import { UserPageComponent } from './containers/user-page';
import { LoginPageComponent } from './containers/login-page';

import { UserLoggedGuard } from './guards/user-logged';
import { NotFoundPageComponent } from './containers/not-found-page';

export const routes: Routes = [
  {
    path: '',
    canActivate: [ UserLoggedGuard ],
    component: UserPageComponent
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: '**',
    component: NotFoundPageComponent
  }
];
