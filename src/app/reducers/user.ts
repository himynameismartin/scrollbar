import { User } from '../models/user';

export interface State {
  loaded: boolean;
  loading: boolean;
  error: boolean;
  user: {};
};

const initialState: State = {
  loaded: false,
  loading: false,
  error: false,
  user: {}
};

export function reducer(state = initialState, action): State {
  switch (action.type) {
    case 'LOGIN_USER': {
      return Object.assign({}, state, {
        loaded: true,
        loading: true,
        error: false,
        user: {}
      });
    }

    case 'LOGIN_USER_SUCCESS': {
      const user = action.payload;

      return Object.assign({}, state, {
        loaded: true,
        loading: false,
        user: user
      });
    }

    case 'LOGIN_USER_FAIL': {
      return Object.assign({}, state, {
        loaded: false,
        loading: false,
        error: true,
        user: {}
      });
    }

    case 'LOGOUT_USER': {
      return Object.assign({}, state, initialState);
    }

    default: {
      return state;
    }
  }
}


export const getLoaded = (state: State) => state.loaded;

export const getLoading = (state: State) => state.loading;

export const getError = (state: State) => state.error;

export const getUser = (state: State) => state.user;
