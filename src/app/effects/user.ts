import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/toArray';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { of } from 'rxjs/observable/of';

import { Store } from '@ngrx/store';
import * as fromRoot from '../reducers';
import * as userActions from '../actions/user';


@Injectable()
export class UserEffects {

  @Effect() login$ = this.actions$
      .ofType('LOGIN_USER')
      .map(action => action.payload)
      .switchMap(credentials => fetch(`https://api.bitbucket.org/1.0/user`, {
          method:'GET',
          headers: {
            'Authorization': 'Basic '+btoa(`${credentials.username}:${credentials.password}`),
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then((res) => (
          res.json().then((val) => (userActions.loginUserSuccess(val.user))
          )
        ))
        .catch((error) => (userActions.loginUserFail()))
      );

  @Effect() redirectOnLogin$ = this.actions$
      .ofType('LOGIN_USER_SUCCESS')
      .switchMap(() => this.router.navigate(['/']));

  @Effect() redirectOnLogout$ = this.actions$
      .ofType('LOGOUT_USER')
      .switchMap(() => this.router.navigate(['/login']));

  constructor(
    private http: Http,
    private actions$: Actions,
    private router: Router,
    private store: Store<fromRoot.State>
  ) { }
}
