import 'rxjs/add/operator/take';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/let';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import * as fromRoot from '../reducers';


/**
 * Guards are hooks into the route resolution process, providing an opportunity
 * to inform the router's navigation process whether the route should continue
 * to activate this route. Guards must return an observable of true or false.
 */
@Injectable()
export class UserLoggedGuard implements CanActivate {
  constructor(
    private store: Store<fromRoot.State>,
    private router: Router
  ) { }

  hasUserInStore(): Observable<boolean> {
    return this.store.select(fromRoot.getUserObject)
      .take(1);
  }

  hasUser(): Observable<boolean> {
    return this.hasUserInStore()
      .switchMap(inStore => {
        const emptyObject = Object.keys(inStore).length === 0;
        if (!emptyObject) {
          return of(!emptyObject);
        }

        return of(false);
      });
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | boolean {
    return this.hasUser().map((val) => {
      if (val) {
        return true;
      }

      this.router.navigate(['/login']);
      return false;
    });
  }
}
