export interface User {
    username: string;
    first_name: string;
    last_name: string;
    display_name: string;
    is_staff: boolean;
    avatar: string;
    resource_uri: string;
    is_team: boolean;
};
