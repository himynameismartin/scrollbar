import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import { Component, Output, Input, EventEmitter } from '@angular/core';

import { Credentials } from '../models/credentials';

@Component({
  selector: 'sb-login',
  template: `
    <md-card>
      <md-card-title>Bitbucket Log In</md-card-title>
      <md-card-content>
        <md-input-container>
          <input mdInput placeholder="Your Bitbucket Login" (keyup)="onUsername($event)">
        </md-input-container>
        <md-input-container>
          <input mdInput placeholder="Your Bitbucket Password" type="password" (keyup)="onPassword($event)">
        </md-input-container>
        <button md-button [disabled]="disableLogin()" (click)="handleClick()">Login</button>
        <md-spinner [class.show]="loading"></md-spinner>
      </md-card-content>
      <md-card-actions>
        <md-error *ngIf="error">
          Error while signing in: please check your credentials or try again later =(
        </md-error>
      </md-card-actions>
    </md-card>
  `,
  styles: [`
    md-card-title,
    md-card-content {
      display: flex;
      justify-content: center;
    }

    input {
      width: 300px;
    }

    md-card-spinner {
      padding-left: 60px; // Make room for the spinner
    }

    md-spinner {
      width: 30px;
      height: 30px;
      position: relative;
      top: 10px;
      left: 10px;
      opacity: 0.0;
    }

    md-spinner.show {
      opacity: 1.0;
    }
  `]
})
export class LoginComponent {
  @Input() loading: boolean = false;
  @Input() error: boolean = false;
  @Output() login = new EventEmitter<Credentials>();

  private username: string = '';
  private password: string = '';

  onUsername(event: any) {
    this.username = event.target.value;
  }

  onPassword(event: any) {
    this.password = event.target.value;
  }

  disableLogin() {
    return !this.username || !this.password;
  }

  handleClick() {
    this.login.emit({username: this.username, password: this.password});
  }
}
