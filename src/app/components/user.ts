import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import { Component, Output, Input, EventEmitter } from '@angular/core';

import { User } from '../models/user';

@Component({
  selector: 'sb-user',
  template: `
    <md-card>
      <md-card-title>Bitbucket User Page</md-card-title>

      <img src={{user?.avatar}} />
      {{user?.username}}

      <md-card-actions>
        <button md-button (click)="handleClick()">Logout</button>
      </md-card-actions>
    </md-card>
  `,
  styles: [`
    md-card-title,
    md-card-content {
      display: flex;
      justify-content: center;
    }
  `]
})
export class UserComponent {
  @Input() user: User;
  @Output() logout = new EventEmitter<any>();

  handleClick() {
    this.logout.emit();
  }
}
