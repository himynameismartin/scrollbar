import 'rxjs/add/operator/let';
import 'rxjs/add/operator/take';
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromRoot from '../reducers';
import * as userActions from '../actions/user';


@Component({
  selector: 'sb-login-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <sb-login [loading]="loading$ | async" [error]="error$ | async" (login)="login($event)"></sb-login>
  `
})
export class LoginPageComponent {
  loading$: Observable<boolean>;
  error$: Observable<boolean>;

  constructor(private store: Store<fromRoot.State>) {
    this.loading$ = store.select(fromRoot.getUserLoading);
    this.error$ = store.select(fromRoot.getUserError);
  }

  login(credentials) {
    this.store.dispatch(userActions.loginUser(credentials));
  }
}
