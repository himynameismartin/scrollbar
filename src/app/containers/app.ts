import 'rxjs/add/operator/let';
import { Observable } from 'rxjs/Observable';
import { Component, ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'bc-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div>
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {

  constructor() {

  }

}
