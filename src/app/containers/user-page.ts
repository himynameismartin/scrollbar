import 'rxjs/add/operator/let';
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromRoot from '../reducers';
import { User } from '../models/user';
import * as userActions from '../actions/user';


@Component({
  selector: 'sb-user-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div *ngIf="user$">
      <sb-user [user]="user$ | async" (logout)="logout($event)"></sb-user>
    </div>
  `,
  styles: [`
    md-card-title {
      display: flex;
      justify-content: center;
    }
  `]
})
export class UserPageComponent {
  user$: Observable<User>;

  constructor(private store: Store<fromRoot.State>) {
    this.user$ = store.select(fromRoot.getUserObject);
  }

  logout() {
    this.store.dispatch(userActions.logoutUser());
  }
}
