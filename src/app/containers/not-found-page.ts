import { Component, ChangeDetectionStrategy } from '@angular/core';


@Component({
  selector: 'sb-not-found-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <md-card>
      <md-card-title>Not Found</md-card-title>
      <md-card-content>
        <p>Now this is awkward. =/</p>
      </md-card-content>
      <md-card-actions>
        <button md-raised-button color="primary" routerLink="/">Take Me Somewhere Safe</button>
      </md-card-actions>
    </md-card>
  `,
  styles: [`
    :host {
      text-align: center;
    }
  `]
})
export class NotFoundPageComponent { }
