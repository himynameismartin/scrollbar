
export function loginUser(payload) {
  return {
    type: 'LOGIN_USER',
    payload
  }
}

export function loginUserSuccess(payload) {
  return {
    type: 'LOGIN_USER_SUCCESS',
    payload
  }
}

export function loginUserFail() {
  return {
    type: 'LOGIN_USER_FAIL'
  }
}

export function logoutUser() {
  return {
    type: 'LOGOUT_USER'
  }
}
